use crate::{get_now, Accounts, AccountsPending, State};
use ldap3::exop::PasswordModify;
use ldap3::result::ExopResult;
use ldap3::{LdapConn, Scope};
use sqlx::{Error, Pool, Sqlite};
use std::collections::HashSet;
use tide::prelude::{json, Deserialize};
use tide::Request;

#[derive(Debug, Deserialize)]
pub struct LdapNewUser {
    user: String,
    // email that is used on wolves
    mail: String,
    name_first: String,
    name_second: String,
}

/// Handles initial detail entering page
pub async fn post_new_account(mut req: Request<State>) -> tide::Result {
    // check if username exists
    //  search ldap and local
    //  send back that that username is in use

    // check local if email exists (periodic sync)
    //  if not then request info on individual user
    //      if there is no email matching still send 200 back
    //      if there is then send email with link to the account

    // save user details in the db

    let LdapNewUser {
        user,
        mail,
        name_first,
        name_second,
    } = req.body_json().await?;

    let config = &req.state().config;

    // easier to give each request its own connection
    let mut ldap = LdapConn::new(&config.ldap_host)?;

    // ldap3 docs say a blank username and pass is an anon bind
    ldap.simple_bind("", "")?.success()?;

    let filter_dn = format!("(uid={})", &user);
    if let Ok(x) = ldap.search("ou=users,dc=skynet,dc=ie", Scope::OneLevel, &filter_dn, vec!["*"]) {
        if let Ok((rs, _res)) = x.success() {
            if !rs.is_empty() {
                return Ok(json!({"result": "error", "error": "username not available"}).into());
            }
        }
    }

    let filter_email = format!("(mail={})", mail);
    if let Ok(x) = ldap.search("ou=users,dc=skynet,dc=ie", Scope::OneLevel, &filter_email, vec!["*"]) {
        if let Ok((rs, _res)) = x.success() {
            if !rs.is_empty() {
                return Ok(json!({"result": "error", "error": "email in use"}).into());
            }
        }
    }

    // done with ldap
    ldap.unbind()?;

    // setup the pool, going to need it for the rest of it
    let pool = &req.state().db;

    db_pending_clear_expired(pool).await?;

    // now check local
    if let Ok(results) = sqlx::query_as::<_, AccountsPending>(
        r#"
        SELECT *
        FROM accounts_pending
        WHERE user == ?
        "#,
    )
    .bind(&user)
    .fetch_all(pool)
    .await
    {
        if !results.is_empty() {
            return Ok(json!({"result": "error", "error": "username not available"}).into());
        }
    }

    if let Ok(results) = sqlx::query_as::<_, AccountsPending>(
        r#"
        SELECT *
        FROM accounts_pending
        WHERE mail == ?
        "#,
    )
    .bind(&mail)
    .fetch_all(pool)
    .await
    {
        if !results.is_empty() {
            return Ok(json!({"result": "error", "error": "email in use"}).into());
        }
    }

    // frontend now tells user to check their email

    /*
       TODO:
           now check with wolves to see if the email is already activated
           use email as primary match
           then search up to see if teh wolves ID has a match
           if not generate tuhe user and send email
    */

    let cn = format!("{} {}", name_first, name_second);
    let auth_code = create_random_string(50);
    // 1 hour expiry
    let expiry = get_now() + (60 * 60);

    sqlx::query_as::<_, AccountsPending>(
        r#"
            INSERT OR REPLACE INTO accounts_pending (user, mail, cn, sn, action, auth_code, expiry)
            VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)
            "#,
    )
    .bind(&user)
    .bind(&mail)
    .bind(&cn)
    .bind(&name_second)
    .bind("account_new")
    .bind(&auth_code)
    .bind(expiry)
    .fetch_optional(pool)
    .await
    .ok();

    // TODO: Send email with auth_code

    Ok(json!({"result": "success"}).into())
}

// clear the db of expired ones before checking for username and validating inputs
async fn db_pending_clear_expired(pool: &Pool<Sqlite>) -> Result<Vec<AccountsPending>, Error> {
    let now = get_now();
    sqlx::query_as::<_, AccountsPending>(
        r#"
        DELETE 
        FROM accounts_pending
        WHERE expiry < ?
        "#,
    )
    .bind(now)
    .fetch_all(pool)
    .await
}

fn create_random_string(length: usize) -> String {
    use rand::distributions::Alphanumeric;
    use rand::{thread_rng, Rng};

    thread_rng().sample_iter(&Alphanumeric).take(length).map(char::from).collect()
}

#[derive(Debug, Deserialize)]
pub struct LdapUserVerify {
    auth_code: String,
    password: String,
}

/// Handles the verification that a user has access to the email
pub async fn post_new_account_confirmation(mut req: Request<State>) -> tide::Result {
    let user_verify: LdapUserVerify = req.body_json().await?;

    let State {
        db,
        config,
        ..
    } = &req.state();

    // setup ldap connection
    let mut ldap = LdapConn::new(&config.ldap_host)?;
    ldap.simple_bind(&config.ldap_admin, &config.ldap_admin_pw)?.success()?;

    // make sure to clear out the expired ones first
    db_pending_clear_expired(db).await?;

    // search db for auth_code
    let results = account_verification_find_pending(db, "account_new", &user_verify.auth_code).await;

    if results.is_empty() {
        return Ok(json!({"result": "error"}).into());
    }

    let user_details = &results[0];
    let uid_number = get_max_uid_number(db).await;

    // create teh new user account in ldap
    account_verification_new_account(&mut ldap, user_details, uid_number).await?;

    // now to properly set teh password
    account_verification_reset_password_admin(&mut ldap, &user_details.user, &user_verify.password)?;

    // done with ldap
    ldap.unbind()?;

    // delete from tmp db
    account_verification_clear_pending(db, &user_verify.auth_code).await?;

    // add new users to teh local database
    account_verification_add_local(db, &user_details.user, uid_number).await?;

    // frontend tells user that initial password ahs been sent to tehm
    Ok(json!({"result": "success"}).into())
}

fn get_sk_created() -> String {
    use chrono::Utc;
    let now = Utc::now();

    format!("{}", now.format("%Y%m%d%H%M%SZ"))
}

async fn get_max_uid_number(db: &Pool<Sqlite>) -> i64 {
    if let Ok(results) = sqlx::query_as::<_, Accounts>(
        r#"
        SELECT *
        FROM accounts
        ORDER BY uid_number DESC
        LIMIT 1
        "#,
    )
    .fetch_all(db)
    .await
    {
        if !results.is_empty() {
            return results[0].uid_number + 1;
        }
    }

    9999
}

async fn account_verification_find_pending(db: &Pool<Sqlite>, action: &str, auth_code: &str) -> Vec<AccountsPending> {
    sqlx::query_as::<_, AccountsPending>(
        r#"
        SELECT *
        FROM accounts_pending
        WHERE auth_code == ? AND action == ?
        "#,
    )
    .bind(auth_code)
    .bind(action)
    .fetch_all(db)
    .await
    .unwrap_or(vec![])
}

async fn account_verification_new_account(ldap: &mut LdapConn, user_details: &AccountsPending, uid_number: i64) -> Result<(), ldap3::LdapError> {
    let AccountsPending {
        user,
        mail,
        cn,
        sn,
        ..
    } = user_details;

    let dn = format!("uid={},ou=users,dc=skynet,dc=ie", user);
    let home_directory = format!("/home/{}", user);
    let password_tmp = create_random_string(50);
    let labeled_uri = format!("ldap:///ou=groups,dc=skynet,dc=ie??sub?(&(objectclass=posixgroup)(memberuid={}))", user);
    let sk_mail = format!("{}@skynet.ie", user);
    let sk_created = get_sk_created();

    // create user
    ldap.add(
        &dn,
        vec![
            ("objectClass", HashSet::from(["top", "person", "posixaccount", "ldapPublicKey", "inetOrgPerson", "skPerson"])),
            // top
            ("ou", HashSet::from(["users"])),
            // person
            ("uid", HashSet::from([user.as_str()])),
            ("cn", HashSet::from([cn.as_str()])),
            // posixaccount
            ("uidNumber", HashSet::from([uid_number.to_string().as_str()])),
            ("gidNumber", HashSet::from(["1001"])),
            ("homedirectory", HashSet::from([home_directory.as_str()])),
            ("userpassword", HashSet::from([password_tmp.as_str()])),
            // inetOrgPerson
            ("mail", HashSet::from([mail.as_str()])),
            ("sn", HashSet::from([sn.as_str()])),
            // skPerson
            ("labeledURI", HashSet::from([labeled_uri.as_str()])),
            ("skMail", HashSet::from([sk_mail.as_str()])),
            // need to get this from wolves
            //("skID", HashSet::from(["12345678"])),
            ("skCreated", HashSet::from([sk_created.as_str()])),
        ],
    )?
    .success()?;

    Ok(())
}

fn account_verification_reset_password_admin(ldap: &mut LdapConn, user: &str, pass: &str) -> Result<ExopResult, ldap3::LdapError> {
    let dn = format!("uid={},ou=users,dc=skynet,dc=ie", user);

    // now to properly set teh password
    let tmp = PasswordModify {
        user_id: Some(&dn),
        old_pass: None,
        new_pass: Some(pass),
    };

    ldap.extended(tmp)
}

async fn account_verification_clear_pending(db: &Pool<Sqlite>, auth_code: &str) -> Result<Vec<AccountsPending>, Error> {
    sqlx::query_as::<_, AccountsPending>(
        r#"
        DELETE FROM accounts_pending
        WHERE auth_code == ?
        "#,
    )
    .bind(auth_code)
    .fetch_all(db)
    .await
}

async fn account_verification_add_local(db: &Pool<Sqlite>, user: &str, uid_number: i64) -> Result<Option<Accounts>, Error> {
    sqlx::query_as::<_, Accounts>(
        "
        INSERT OR REPLACE INTO accounts (user, uid_number, enabled)
        VALUES (?1, ?2, ?3)
        ",
    )
    .bind(user)
    .bind(uid_number)
    .bind(false)
    .fetch_optional(db)
    .await
}
