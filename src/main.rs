use skynet_ldap_server::methods::account_new::{post_new_account, post_new_account_confirmation};
use skynet_ldap_server::methods::account_update::post_update_ldap;
use skynet_ldap_server::{db_init, get_config, State};

#[async_std::main]
async fn main() -> tide::Result<()> {
    let config = get_config();
    let db = db_init(&config).await?;

    let host_port = config.host_port.clone();

    tide::log::start();

    let state = State {
        db,
        config,
    };

    let mut app = tide::with_state(state);

    app.at("/ldap/update").post(post_update_ldap);

    app.at("/ldap/new").post(post_new_account);
    app.at("/ldap/new/verify").post(post_new_account_confirmation);

    app.listen(host_port).await?;
    Ok(())
}

/* Create new account


  1. Check if ID is available
  2. Ask user to fill in:
    * uid
    * First Name
    * Surname Name
    * Wolves email
  3. Email + link is sent to wolves email
    * only if its paid up and it hasn't been used before
  4. Ldap entry created
  5. Email with initial pw is sent to user
  6. Account added to skynet-users (they are paid up)

*/

/* Join existing account to wolves
   related to above

*/

/* Password reset via email

*/

/* script to pull in all active members from wolves
   update the groups
   check if there are any pending signups

*/
